/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.edu.sgstar.sgstaredu;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.edu.sgstar.sgstaredu";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 19;
  public static final String VERSION_NAME = "2.0.4";
}

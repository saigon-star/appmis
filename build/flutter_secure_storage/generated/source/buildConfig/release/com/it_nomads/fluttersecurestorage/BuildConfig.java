/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.it_nomads.fluttersecurestorage;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.it_nomads.fluttersecurestorage";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 6;
  public static final String VERSION_NAME = "3.3.1";
}

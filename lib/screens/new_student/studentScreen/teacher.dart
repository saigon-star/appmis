import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class TeacherScreen extends StatefulWidget {
  const TeacherScreen({key}) : super(key: key);
  @override
  _TeacherScreenState createState() => _TeacherScreenState();
}

class _TeacherScreenState extends State<TeacherScreen> {
  List listSearch;
  List<dynamic> listTeacher;
  String teacherPhoto;
  Future<void> _launched;
  bool hasData=false;
  void initState() {
    this.getTeacher();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final items = List<String>.generate(5, (index) => null);

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBarWidget(),
        body: hasData!=false? Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20,top: 20.0),
                child: Center(
                  child: Container(
                    width: double.infinity,
                    child: new Text(
                      "teacher contact".toUpperCase(),
                      style: new TextStyle(
                        color: Color(0Xff13438f),
                        fontWeight: FontWeight.w600,
                        fontSize: 24,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    decoration: new BoxDecoration(
                      borderRadius:
                          new BorderRadius.all(new Radius.circular(30.0)),
                      color: Color(0xff7cd3f7),
                    ),
                    padding: new EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                  ),
                ),
              ),
              SizedBox(height: 25),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide:
                              BorderSide(color: Color(0xff7cd3f7), width: 2)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide:
                              BorderSide(color: Color(0xff7cd3f7), width: 2)),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color(0xff13438f),
                        size: 30,
                      ),
                      labelText: 'search'.toUpperCase(),
                      labelStyle: TextStyle(color: Color(0xff13438f))),
                  onChanged: (value){
                    listSearch = [];
                    listTeacher.forEach((element) {
                      var title = element["full_name"];
                      if (title
                          .toLowerCase()
                          .contains(value.toLowerCase())) {
                        setState(() {
                          listSearch.add(element);
                        });
                      }
                    });
                    if (value == '') {
                      setState(() {
                        listSearch = null;
                      });
                    }
                  },
                ),
              ),
              Expanded(
                  child: ListView.builder(
                      // itemCount: listTeacher==null?0:listTeacher.length,
                    itemCount: listSearch==null?listTeacher.length:listSearch.length,

                      itemBuilder: (BuildContext context, int index) {

                        return Column(
                          children: [
                            SizedBox(height: 20),
                            Padding(
                              padding: const EdgeInsets.only(left: 20, right: 20),
                              child: Container(
                                height: 140,
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xff7cd3f7), width: 2))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: 60,
                                      height: 60,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            width: 2,
                                            color: const Color(0xff7cd3f7),
                                          ),
                                          shape: BoxShape.circle,
                                          color: Colors.white),
                                      child: CircleAvatar(
                                        backgroundImage: listSearch==null?NetworkImage('https://sgstar.edu.vn/'+ listTeacher[index]['staff_photo'].toString()):NetworkImage('https://sgstar.eedu.vn/'+ listSearch[index]['staff_photo'].toString()),
                                        backgroundColor: Colors.white,
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          listSearch==null? Text(listTeacher[index]['full_name'],
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w700),
                                            overflow: TextOverflow.ellipsis,):Text(listSearch[index]['full_name'],
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w700),
                                            overflow: TextOverflow.ellipsis,),

                                          SizedBox(
                                            height: 5,
                                          ),
                                          listSearch==null? Text("Subject name: "+listTeacher[index]['subject_name']):Text("Subject name: "+listSearch[index]['subject_name']),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            'Read more',
                                            style: TextStyle(
                                                color: Color(0xff13438f),
                                                fontStyle: FontStyle.italic),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              InkWell(
                                                onTap:(){
                                                  listSearch==null? launch("tel://"+listTeacher[index]['mobile']):launch("tel://"+listSearch[index]['mobile']);
                                                },
                                                child: Container(
                                                  width: 50,
                                                  height: 50,
                                                  padding: EdgeInsets.all(5),
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Colors.white),
                                                  child: Image.asset(
                                                      'assets/images/icons/call.png'),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    listSearch==null?_openUrl('sms:' + listTeacher[index]['mobile']):_openUrl('sms:' + listSearch[index]['mobile']);
                                                  });
                                                },
                                                child: Container(
                                                  width: 50,
                                                  height: 50,
                                                  padding: EdgeInsets.all(5),
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Colors.white),
                                                  child: Image.asset(
                                                      'assets/images/icons/chatgreen.png'),
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    listSearch==null?_openUrl('sms:' + listTeacher[index]['email']):_openUrl('sms:' + listSearch[index]['email']);
                                                    });
                                                },
                                                child: Container(
                                                  width: 50,
                                                  height: 50,
                                                  padding: EdgeInsets.all(5),
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Colors.white),
                                                  child: Image.asset(
                                                      'assets/images/icons/mailbox.png'),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        );
                      })),
            ],
          ),
        ):Container());
  }
  Future<String> getTeacher() async{
    final pref = await SharedPreferences.getInstance();
    String idUser = pref.get('StudentId');
    final response = await http.get(Uri.parse(InfixApi.getTeacherList(int.parse(idUser))));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      hasData=true;
      listTeacher = map["data"]["teacher_list"];
    });
    print(listTeacher);

    return "Success!";
  }
  Future<void> _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

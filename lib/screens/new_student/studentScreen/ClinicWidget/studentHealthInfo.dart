import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class StudentHealthInfo extends StatefulWidget {
  const StudentHealthInfo({key}) : super(key: key);

  @override
  _StudentHealthInfoState createState() => _StudentHealthInfoState();
}

class _StudentHealthInfoState extends State<StudentHealthInfo> {
  String bloodPressure;
  String heartBeat;
  String rightEye;
  String leftEye;
  String height;
  String weight;
  String bloodGroup;
  bool hasData=false;
  @override
  void initState() {
    // this.getHealthMonitor();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return Container(
      padding: EdgeInsets.all(12.0),
      color: Colors.white,
      child: GridView.count(
        // childAspectRatio: (100 / 165),
          primary: true,
          crossAxisSpacing: 15,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFF9EDEFF)),
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Image(
                      image:
                      AssetImage('assets/images/icons/bloodPressure.png'),
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Blood pressure: "),
                        Text(bloodPressure!=null?bloodPressure.toString():'0'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFF9EDEFF)),
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Image(
                      image: AssetImage('assets/images/icons/heartBeat.png'),
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Heart Beat: "),
                        Text(heartBeat!=null?heartBeat.toString():'0'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFF9EDEFF)),
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Image(
                      image: AssetImage('assets/images/icons/eye.png'),
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Right eye: "),
                        Text(rightEye!=null?rightEye.toString():'0'),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Left eye: "),
                        Text(leftEye!=null?leftEye.toString():'0'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFF9EDEFF)),
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Image(
                      image: AssetImage('assets/images/icons/height.png'),
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Height: "),
                        Text(height!=null?height.toString():'0'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFF9EDEFF)),
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Image(
                      image: AssetImage('assets/images/icons/weight.png'),
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Weight: "),
                        Text(weight!=null?weight.toString():'0'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFF9EDEFF)),
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Image(
                      image: AssetImage('assets/images/icons/bloodGroup.png'),
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Blood group: "),
                        Text(bloodGroup!=null?bloodGroup.toString():'0'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ]),
    );

  }

  Future<void> getHealthMonitor() async {
    final pref = await SharedPreferences.getInstance();
    String idStudent = pref.get('StudentId');
    final response = await http.get(Uri.parse(InfixApi.getHealthMonitor(int.parse(idStudent))));
    var jsonData = json.decode(response.body);
    if (mounted) {
      setState(() {
         bloodPressure=jsonData['data']['health']['blood_pressure'].toString();
         heartBeat=jsonData['data']['health']['heart_beat'].toString();
         rightEye=jsonData['data']['health']['right_eye'].toString();
         leftEye=jsonData['data']['health']['left_eye'].toString();
         height=jsonData['data']['health']['height'].toString();
         weight=jsonData['data']['health']['weight'].toString();
         bloodGroup=jsonData['data']['health']['blood_group'].toString();
        hasData=true;
      });
    }
    // print(bloodPressure);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/date_of_week_widget.dart';
import 'package:infixedu/screens/new_student/studentScreen/Widgets/Absent/AbsentList.dart';
import 'package:infixedu/screens/new_student/studentScreen/Widgets/Absent/viewAbsent.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:intl/intl.dart';

class AbsentScreen extends StatefulWidget {
  const AbsentScreen({key}) : super(key: key);
  @override
  _AbsentScreenState createState() => _AbsentScreenState();
}

class _AbsentScreenState extends State<AbsentScreen> {
  bool _checkbox = false;
  bool _checkbox1 = false;
  bool _checkbox2 = false;
  bool _checkbox3 = false;
  String name;
  var _starttime;
  TextEditingController _controllerInput_1 = TextEditingController();
  void initState() {
    super.initState();
    name = getName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(),
      body: teacherAbsent()
    );
  }

  Widget studentAbsent(){
    return ListView(
      children: [
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Center(
            child: Container(
              width: double.infinity,
              child: new Text(
                "absent".toUpperCase(),
                style: new TextStyle(
                  color: Color(0Xff13438f),
                  fontWeight: FontWeight.w600,
                  fontSize: 24,
                ),
                textAlign: TextAlign.center,
              ),
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
                color: Color(0xff7cd3f7),
              ),
              padding: new EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            ),
          ),
        ),
        DateOfWeekWidget(),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 15, left: 20),
              child: Text(
                "list of courses".toUpperCase(),
                style: new TextStyle(
                  color: Color(0Xff13438f),
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  //                    <--- top side
                  color: Color(0xff7cd3f7),
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: Text('16/08/2021')),
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Color(0xff7cd3f7),
                  ),
                  child: Checkbox(
                    value: _checkbox,
                    onChanged: (value) {
                      setState(() {
                        _checkbox = !_checkbox;
                        _checkbox1 = !_checkbox;
                      });
                    },
                  ),
                ),
                Text('Absent'),
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Color(0xff7cd3f7),
                  ),
                  child: Checkbox(
                    value: _checkbox1,
                    onChanged: (value) {
                      setState(() {
                        _checkbox1 = !_checkbox1;
                        _checkbox = !_checkbox1;
                      });
                    },
                  ),
                ),
                Text('Present'),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  //                    <--- top side
                  color: Color(0xff7cd3f7),
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              children: [
                Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: Text('17/08/2021')),
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Color(0xff7cd3f7),
                  ),
                  child: Checkbox(
                    value: _checkbox2,
                    onChanged: (value) {
                      setState(() {
                        _checkbox2 = !_checkbox2;
                        _checkbox3 = !_checkbox2;
                      });
                    },
                  ),
                ),
                Text('Absent'),
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Color(0xff7cd3f7),
                  ),
                  child: Checkbox(
                    value: _checkbox3,
                    onChanged: (value) {
                      setState(() {
                        _checkbox3 = !_checkbox3;
                        _checkbox2 = !_checkbox3;
                      });
                    },
                  ),
                ),
                Text('Present'),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 3),
          child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                  side: BorderSide(color: Color(0xff7cd3f7), width: 2.0)),
              child: Padding(
                padding: EdgeInsets.only(top: 10, left: 15),
                child: TextField(
                  style: TextStyle(color: Colors.black45),
                  maxLines: 8,
                  decoration: InputDecoration.collapsed(
                      hintText: "Reason Absent ...",
                      hintStyle: TextStyle(
                          fontSize: 14.0, color: Color(0xffd8d8d8))),
                ),
              )),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 25, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text('Attach file',
                  style: TextStyle(color: Color(0xff13438f), fontSize: 16)),
            ],
          ),
        ),
        Center(
          child: ElevatedButton(
            onPressed: () {},
            child: Text(
              'SUBMIT',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w800),
            ),
            style: ElevatedButton.styleFrom(
              primary: Color(0xffe4087e),
              fixedSize: Size(140, 45),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20), // <-- Radius
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget teacherAbsent(){
    return Center(
      child: Container(
        padding: EdgeInsets.only(left: 20.0,right: 20.0),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 70,top: 15.0),
              child: Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height*0.075,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                  color: Color(0xFF9EDEFF),
                  borderRadius: BorderRadius.circular(
                    45.0,
                  ),
                ),
                child: Center(
                  child: Text(
                    'student attendance'.toUpperCase(),
                    style: TextStyle(
                        fontSize: 25,
                        color: Color(0xFF144385),
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            SizedBox(height: 25.0,),
            Align(
              alignment: Alignment.topLeft,
                child: Text('Select date',style: TextStyle(color: Color(0xFF144385),fontSize: 15.0),)),
            SizedBox(height: 15.0,),
            Container(
              width: double.infinity,
              height: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(
                      color: Color(0xff7cd3f7), width: 2)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  onTap: () {
                    showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2999),
                      builder:
                          (BuildContext context, Widget child) {
                        return Theme(
                          data: ThemeData.light().copyWith(
                              primaryColor:
                              const Color(0xff7cd3f7),
                              //Head background
                              accentColor: const Color(
                                  0xff7cd3f7) //selection color
                            //dialogBackgroundColor: Colors.white,//Background color
                          ),
                          child: child,
                        );
                      },
                    ).then((selectedDate) {
                      if (selectedDate != null) {
                        _starttime =
                            selectedDate.millisecondsSinceEpoch;

                        _controllerInput_1.text =
                            DateFormat('dd-MM-yyyy')
                                .format(selectedDate)
                                .toString();
                      }
                    });
                  },
                  readOnly: true,
                  controller: _controllerInput_1,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      labelStyle:
                      TextStyle(color: Color(0xFF7dd3f7)),
                      hintText: "Select date",
                      hintStyle:
                      TextStyle(color: Color(0xFF7dd3f7))),
                ),
              ),
            ),
            SizedBox(height: 15.0,),
            OutlinedButton(
              style: OutlinedButton.styleFrom(
                  primary: Color(0xff7cd3f7)
              ),
              child: Text('Go to attendances check',style: TextStyle(color: Color(0xff7cd3f7) ),),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AbsentList(date: _controllerInput_1.text)),
                );
              },
            ),
            OutlinedButton(
              style: OutlinedButton.styleFrom(
                  primary: Color(0xff7cd3f7)
              ),
              child: Text('View list',style: TextStyle(color: Color(0xff7cd3f7) ),),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => AbsentViewList(date: _controllerInput_1.text)),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
  String getName() {
    Utils.getStringValue('full_name').then((value) {
      setState(() {
        name = value;
      });
    });
    return name;
  }
}

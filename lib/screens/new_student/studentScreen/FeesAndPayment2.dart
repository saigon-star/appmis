import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/CardHeader.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:infixedu/screens/new_student/studentScreen/FeesAndPayment3.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class FeesAndPayment2 extends StatefulWidget {
  final int id;
  const FeesAndPayment2({Key key, this.id}) : super(key: key);

  @override
  _FeesAndPayment2State createState() => _FeesAndPayment2State();
}

class _FeesAndPayment2State extends State<FeesAndPayment2> {
  List<dynamic> payment;
  CarouselController buttonCarouselController = CarouselController();
  final item = List<String>.generate(5, (i) => "Tuition fees");

  @override
  void initState() {
    this.getPaymentDetail();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            CardHeader(
                child: Text(
              'TUITION FEES IN 2020-2021',
              style: TextStyle(color: Colors.white),
            )),
            SizedBox(
              height: 20,
            ),
            Container(
                height: 310,
                padding: EdgeInsets.only(left: 20, right: 20),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: payment!=null?payment.length:0,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: EdgeInsets.only(left: 20.0,right: 20.0),
                      child: Container(
                          width: MediaQuery.of(context).size.width*0.75,
                          // margin: EdgeInsets.symmetric(horizontal: 5.0),
                          decoration: BoxDecoration(
                              border:
                                  Border.all(width: 2, color: Color(0xFF7dd3f7)),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(15),
                                bottomRight: Radius.circular(15),
                                topRight: Radius.circular(15),
                                topLeft: Radius.circular(15),
                              )),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: Color(0xFF7dd3f7),
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      topLeft: Radius.circular(10),
                                    )),
                                width: double.infinity,
                                height: 70,
                                child: Center(
                                  child: Text(
                                    "PAYMENT "+(index+1).toString(),
                                    style: TextStyle(
                                        fontSize: 25.0, color: Colors.white),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 50,
                              ),
                              Text('Payment Id: ' + payment[index]['id_fee_list'],
                                  style: TextStyle(
                                      fontSize: 25.0,
                                      color: Color(0xff07509d),
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 30,
                              ),
                              Text("Paid: "+NumberFormat.simpleCurrency(locale: 'vi').format(int.parse(payment[index]['paid']))),
                              SizedBox(
                                height: 30,
                              ),
                              Text(NumberFormat.simpleCurrency(locale: 'vi').format(int.parse(payment[index]['amount_payment'])) ,
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      color: Color(0xff07509d),
                                      fontWeight: FontWeight.bold)),
                              SizedBox(
                                height: 30,
                              ),
                              Center(
                                child: Text('Deadline: ' + payment[index]['due_date'],
                                    style: TextStyle(color: Color(0xffe4087e), fontSize: 18)),
                              ),
                            ],
                          )),
                    );
                  },
                )),
            SizedBox(
              height: 80,
            ),

            Center(
              child: ElevatedButton(
                  onPressed: () {
                    pushNewScreen(
                      context,
                      screen: FeesAndPayment3(),
                      withNavBar: true, // OPTIONAL VALUE. True by default.
                      pageTransitionAnimation:
                          PageTransitionAnimation.cupertino,
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      'NEXT',
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Color(0xFF7dd3f7)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7.0),
                      )))),
            ),
          ],
        ),
      ),
    );
  }

  Future<String> getPaymentDetail() async {
    final pref = await SharedPreferences.getInstance();
    String id = pref.get('id');


    final response =
    await http.get(Uri.parse(InfixApi.getPaymentDetail(this.widget.id)));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      payment = map["data"]["paymentDetail"];
    });
    // print(payment);
    return "Success!";
  }
}

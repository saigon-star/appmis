import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/studentScreen/Widgets/CommentDetail.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CommentList extends StatefulWidget {
  const CommentList({key}) : super(key: key);

  @override
  _CommentListState createState() => _CommentListState();
}

class _CommentListState extends State<CommentList> {
  String httpUrl = 'https://sgstar.edu.vn/';
  List<dynamic> listDaily;
  final items = List<String>.generate(10, (i) => "Item $i");

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20),
      child: FutureBuilder(
          future: getDaily(),
          builder: (context, data) {
            if (data.hasData != false) {
              return ListView.builder(
                itemCount: listDaily != null ? listDaily.length : 0,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 10.0, bottom: 25.0),
                    child: Card(
                      color: Color(0xffebf6ff),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: ListTile(
                          contentPadding: EdgeInsets.only(left: 0.0),
                          isThreeLine: true,
                          leading: Container(
                            decoration: BoxDecoration(
                              color: Color(0xFF7dd3f7),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30.0),
                                bottomRight: Radius.circular(30.0),
                              ),
                            ),
                            width: 35,
                            height: 30,
                            child: Center(
                                child: Text(
                              (index + 1).toString(),
                              style:
                                  TextStyle(fontSize: 18, color: Colors.white),
                            )),
                          ),
                          title: Text(listDaily[index]['title'].toUpperCase()),
                          subtitle: Column(
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(listDaily[index]['date'])),
                              Padding(
                                padding: EdgeInsets.only(
                                    right: 20.0, top: 5.0, bottom: 10.0),
                                child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: FittedBox(
                                      child: Image.network(
                                          httpUrl + listDaily[index]["image"]),
                                      fit: BoxFit.fill,
                                    )),
                              ),
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: InkWell(
                                    onTap: () {
                                      pushNewScreen(
                                        context,
                                        screen: CommentDetail(
                                            id: listDaily[index]['id']
                                                .toString()),
                                        withNavBar: true,
                                        // OPTIONAL VALUE. True by default.
                                        pageTransitionAnimation:
                                            PageTransitionAnimation.cupertino,
                                      );
                                    },
                                    child: Text(
                                      'Read more',
                                      style: TextStyle(
                                          color: Color(0xFF144385),
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 25,
                                          child: IconButton(
                                              padding: EdgeInsets.all(0),
                                              icon: Image.asset(
                                                'assets/images/icons/4. LIKE.png',
                                                width: 20,
                                                height: 20,
                                              ),
                                              color: Colors.grey,
                                              onPressed: () {
                                                dailyLiked(listDaily[index]['id']);
                                              }),
                                        ),
                                        Text(listDaily[index]['like_count'].toString()),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 25,
                                          child: IconButton(
                                              padding: EdgeInsets.all(0),
                                              icon: Image.asset(
                                                'assets/images/icons/5. COMMENT.png',
                                                width: 20,
                                                height: 20,
                                              ),
                                              color: Colors.grey,
                                              tooltip: 'Comment',
                                              onPressed: () {}),
                                        ),
                                        Text(listDaily[index]['comment_count'].toString(),
                                          textAlign: TextAlign.right,
                                        ),
                                        Text(
                                          ' Comment',
                                          textAlign: TextAlign.right,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ),
                  );
                },
              );
            } else {
              return Container(
                  color: Colors.white,
                  child: Center(child: CircularProgressIndicator()));
            }
          }),
    );
  }

  Future<String> getDaily() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    String role = pref.get('rule');

    final response =
        await http.get(Uri.parse(InfixApi.getDaily(int.parse(userId))));
    // print(Uri.parse(InfixApi.getDaily(int.parse(userId))));
    Map<String, dynamic> map = json.decode(response.body);

    setState(() {
      listDaily = map['data']['daily'];
    });

    return "Success!";
  }

  Future<String> dailyLiked(int id) async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    // print(Uri.parse(InfixApi.dailyLike(int.parse(this.widget.id))));
    final response = await http.get(
        Uri.parse(InfixApi.dailyLike(id,int.parse(userId))));
    getDaily();
    return "Success!";
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/studentScreen/Widgets/AttendanceDetail.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:infixedu/screens/new_student/CommonWidgets/WeekCalendarWidget.dart' as global;

class AttendanceList extends StatefulWidget {
  const AttendanceList({key}) : super(key: key);

  @override
  _AttendanceListState createState() => _AttendanceListState();
}

class _AttendanceListState extends State<AttendanceList> {
  String attendanceType;
  String attendanceDate;
  final items = List<String>.generate(10, (i) => "Item $i");
  @override
  void initState() {
    getHomeWork();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20),
      child: ListView.builder(
        itemCount: attendanceType!=null?1:0,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: InkWell(
              onTap: (){
                pushNewScreen(
                  context,
                  screen: AttendanceDetail(),
                  withNavBar: true, // OPTIONAL VALUE. True by default.
                  pageTransitionAnimation: PageTransitionAnimation.cupertino,
                );
              },
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius:
                    BorderRadius.all(Radius.circular(15))),
                margin: EdgeInsets.all(10),
                color: Color(0xFFEBF6FF),
                shadowColor: Colors.blueGrey,
                elevation: 10,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                     ListTile(
                      title: Text(attendanceDate!=null?attendanceDate:'',
                        style: TextStyle(fontSize: 20),
                      ),
                       subtitle: Text(
                           attendanceType!=null?"Attendance type: " + attendanceTypeDefine(attendanceType).toString():''
                       ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Future<String> getHomeWork() async{
    final pref = await SharedPreferences.getInstance();
    String studentId = pref.get('StudentId');
    String role = pref.get('rule');
    // print(role);
    if(int.parse(role)==2){
      final response = await http.get(Uri.parse(InfixApi.studentAttendanceGet(int.parse(studentId),global.today)));
      // print(Uri.parse(InfixApi.studentAttendanceGet(int.parse(studentId),global.today)));
      Map<String, dynamic> map = json.decode(response.body);
      if(map['data']['attendanceList']!=null) {
        setState(() {
          attendanceType = map['data']['attendanceList']['attendance_type'];
          attendanceDate = map['data']['attendanceList']['attendance_date'];
        });
      }
    }

    return "Success!";
  }

  String attendanceTypeDefine(String type){
    String attendanceTypeDefine;
    print(type);
    if(type=='P')
        attendanceTypeDefine='Present';
    else if(type== 'A')
      return attendanceTypeDefine="absent";
    print(attendanceTypeDefine);
    return attendanceTypeDefine;

  }
}
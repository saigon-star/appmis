import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_summernote/flutter_summernote.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/CardHeader.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:http/http.dart' as http;
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StoreDaily extends StatefulWidget {
  const StoreDaily({key}) : super(key: key);

  @override
  _StoreDailyState createState() => _StoreDailyState();
}

class _StoreDailyState extends State<StoreDaily> {
  GlobalKey<FlutterSummernoteState> _keyEditor = GlobalKey();
  TextEditingController _controllerInput_1 = TextEditingController();
  bool hasData = false;
  bool isSending = false;
  File image;
  var _image;
  String fileName;
  var bodyContent;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              CardHeader(child: Text('DAILY ACTIVITIES')),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: TextField(
                  maxLines: null,
                  controller: _controllerInput_1,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xff7cd3f7), width: 2.0),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xff7cd3f7), width: 2.0),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    label: Text('Title',
                        style: TextStyle(color: Color(0xFF9EDEFF))),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 5),
                child: Container(
                    child: FlutterSummernote(
                      height: 500,
                      value: '',
                      key: _keyEditor,
                      hasAttachment: false,
                      customToolbar: """
                          [
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['insert', ['link', 'table', 'hr']]
                          ]
                        """,
                    ),
                    margin: MediaQuery.of(context).viewInsets),
              ),
              Container(
                padding: EdgeInsets.only(left: 20.0,right: 20.0),
                width: double.infinity,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    shape: StadiumBorder(),
                    side: BorderSide(
                      width: 2,
                      color: Color(0xff7cd3f7),
                    ),
                  ),
                  child: Text(
                    '+ Add File',
                    style: TextStyle(
                        color: Color(0xff13438f),
                        fontSize: 16,
                        fontWeight: FontWeight.w700),
                  ),
                  onPressed: () async {
                    getImage();
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20.0,right: 20.0),
                child: image!=null? Image.file(image):Container(),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 25.0),
                child: Center(
                    child: Container(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      textStyle: TextStyle(fontSize: 24),
                      maximumSize: Size.fromHeight(72),
                      shape: StadiumBorder(),
                    ),
                    onPressed: () async {
                        bodyContent = await _keyEditor.currentState.getText();
                        bodyContent = await _keyEditor.currentState.getText();
                        print(bodyContent);
                        // addNote();
                        storeActivities();
                        if (isSending) return;
                        setState(() {
                          isSending = true;
                        });
                        await Future.delayed(Duration(seconds: 60));
                    },
                    child: isSending
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  )),
                              const SizedBox(
                                width: 24,
                              ),
                              Text('Please Wait...')
                            ],
                          )
                        : Text('Save'),
                  ),
                )),
              ),
            ],
          ),
        ),
      ),
    );
  }


  void _upload(file) async {
    fileName = file.split('/').last;

    FormData data = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        file,
        filename: fileName,
      ),
    });

    Dio dio = new Dio();

    dio
        .post("https://sgstar.edu.vn/api/upload-image-daily", data: data)
        .then((response) => print(response))
        .catchError((error) => print(error));
  }

  Future getImage() async {

    final picker = ImagePicker();

    // var _pickedFile = await picker.pickImage(
    //   source: ImageSource.gallery,
    //   imageQuality: 100, // <- Reduce Image quality
    // );
    var _pickedFile = await FilePicker.platform.pickFiles();

    final temporaryImage=File(_pickedFile.files.first.path);

    setState(() {
      this.image=temporaryImage;
      _image = _pickedFile.files.first.path;
    });
  }

  Future<String> storeActivities() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    _upload(_image);
    print(Uri.parse(InfixApi.storeDailyActivities(int.parse(userId),fileName,_controllerInput_1.text,bodyContent.toString())));
    final response = await http.get(Uri.parse(InfixApi.storeDailyActivities(int.parse(userId),fileName,_controllerInput_1.text,bodyContent.toString())));



    setState(() {
      _controllerInput_1.text = '';
    });
    Navigator.pop(context);
    return "Success!";
  }

}

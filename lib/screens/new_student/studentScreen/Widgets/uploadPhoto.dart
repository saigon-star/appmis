import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';

import 'package:infixedu/utils/apis/Apis.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class UploadPhoto extends StatefulWidget {
  const UploadPhoto({key}) : super(key: key);
  @override
  _UploadPhotoState createState() => _UploadPhotoState();
}

class _UploadPhotoState extends State<UploadPhoto> {
  Future myFuture;
  List<dynamic> leaveTypeList;
  bool hasData = false;

  TextEditingController _textController = TextEditingController();

  var _image;

  void initState() {
    myFuture = this.getLeaveTypeList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(),
      body: FutureBuilder(
        future: myFuture,
        builder: (context, snapshot) {
          if (snapshot.hasData)
            return ListView(
              children: [
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Center(
                    child: Container(
                      width: double.infinity,
                      child: new Text(
                        "Upload Photo".toUpperCase(),
                        style: new TextStyle(
                          color: Color(0Xff13438f),
                          fontWeight: FontWeight.w600,
                          fontSize: 24,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      decoration: new BoxDecoration(
                        borderRadius:
                            new BorderRadius.all(new Radius.circular(30.0)),
                        color: Color(0xff7cd3f7),
                      ),
                      padding: new EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 20, 3),
                  child: TextField(
                    maxLines: null,
                    controller: _textController,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff7cd3f7), width: 2.0),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff7cd3f7), width: 2.0),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      label: Text(
                        'Title',
                        style: TextStyle(color: Color(0xFF9EDEFF)),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      shape: StadiumBorder(),
                      side: BorderSide(
                        width: 2,
                        color: Color(0xff7cd3f7),
                      ),
                    ),
                    child: Text(
                      '+ Take A Picture',
                      style: TextStyle(
                          color: Color(0xff13438f),
                          fontSize: 16,
                          fontWeight: FontWeight.w700),
                    ),
                    onPressed: () async {
                      getImage();
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                      child:
                          Text(_image != null ? _image.split('/').last : '')),
                ),
                Center(
                  child: ElevatedButton(
                    onPressed: () async {
                      if (_textController.text == '') {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                  title: Text('Please fill the title'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context, 'Cancel');
                                      },
                                      child: const Text('OK'),
                                    ),
                                  ],
                                ));
                      } else if (_image == null) {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                  title: Text('Please add your photo'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context, 'Cancel');
                                      },
                                      child: const Text('OK'),
                                    ),
                                  ],
                                ));
                      } else {
                        _upload(_image);
                        storeImagePost();
                      }
                    },
                    child: Text(
                      'SUBMIT',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w800),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Color(0xffe4087e),
                      fixedSize: Size(140, 45),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20), // <-- Radius
                      ),
                    ),
                  ),
                ),
              ],
            );
          else
            return Container(
                color: Colors.white,
                child: Center(child: CircularProgressIndicator()));
        },
      ),
    );
  }

  Future<String> getLeaveTypeList() async {
    return "Success!";
  }

  Future<String> storeImagePost() async {
    final pref = await SharedPreferences.getInstance();

    String id = pref.get('id');
    String title = _textController.text;
    String fileName = _image.split('/').last;

    final response = await http.get(
        Uri.parse(InfixApi.storeImagePost(title, int.tryParse(id), fileName)));

    return "Success!";
  }

  void _upload(file) async {
    String fileName = file.split('/').last;

    FormData data = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        file,
        filename: fileName,
      ),
    });

    Dio dio = new Dio();

    dio
        .post("https://sgstar.edu.vn/api/upload-image-post", data: data)
        .then((response) => print(response))
        .catchError((error) => print(error));
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text('Upload successfully'),
            ));
    Navigator.of(context).popUntil((route) => route.isFirst);
  }

  Future getImage() async {
    final picker = ImagePicker();

    var _pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 65);
    setState(() {
      _image = _pickedFile.path;
    });
  }
}

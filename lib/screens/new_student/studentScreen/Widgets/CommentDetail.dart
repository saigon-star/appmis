import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/CardHeader.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class CommentDetail extends StatefulWidget {
  final String id;

  const CommentDetail({key, this.id}) : super(key: key);

  @override
  _CommentDetailState createState() => _CommentDetailState();
}

class _CommentDetailState extends State<CommentDetail> {
  List<dynamic> commentList;
  TextEditingController commentController = TextEditingController();
  String title;
  String content;
  String image;
  String date;
  String likeCount;
  String commentCount;
  String httpUrl = 'https://sgstar.edu.vn/';

  @override
  void initState() {
    getDailyComment();
    getDaily();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(),
        body:
        FutureBuilder(
            future: getDaily(),
            builder: (context, data) {
              if (data.hasData != false) {
                return SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height*0.5,
                        child: ListView(
                          children: [
                            Container(
                              //height: MediaQuery.of(context).size.height,
                              child: Card(
                                color: Color(0xffebf6ff),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(title != null ? title : ''
                                      ),
                                      subtitle: Text(date != null ? date : ''),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 15, right: 15, bottom: 20),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Image.network(
                                                  httpUrl +
                                                      image),
                                              Text(
                                                  content != null ? content : ''
                                                  , style: TextStyle(
                                                  color: Colors.grey[400]
                                              ), textAlign: TextAlign.justify),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment
                                                    .spaceBetween,
                                                children: [
                                                  Row(
                                                    children: [
                                                      IconButton(
                                                          icon: Image.asset(
                                                              'assets/images/icons/4. LIKE.png'),
                                                          color: Colors.grey,
                                                          tooltip: 'Like',
                                                          onPressed: () {
                                                            dailyLiked();
                                                          }
                                                      ),
                                                      Text(likeCount != null ? likeCount
                                                          .toString() : '0')
                                                    ],
                                                  ),
                                                  Row(
                                                    children: [
                                                      IconButton(
                                                          icon: Image.asset(
                                                              'assets/images/icons/5. COMMENT.png'),
                                                          color: Colors.grey,
                                                          tooltip: 'Comments',
                                                          onPressed: () {}),
                                                      Text(commentCount!=null?commentCount.toString():'0', style: TextStyle(
                                                          color: Color(0xFF144385)),),
                                                      Text(' Comment', style: TextStyle(
                                                          color: Color(0xFF144385)),)
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),


                          ],
                        ),
                      ),

                      Container(
                        height: MediaQuery.of(context).size.height*0.35,
                        child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                                child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text('Parents comment', style: TextStyle(
                                        color: Color(0xFF144385),
                                        fontWeight: FontWeight.bold),)),
                              ),
                              Expanded(
                                child: ListView.builder(
                                  itemCount: commentList!=null?commentList.length:0,
                                  itemBuilder: (context, index) {
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child:
                                        Card(
                                            child: ListTile(
                                              title: Text(commentList[index]['full_name']),
                                              subtitle: Text(commentList[index]['comment_content'],),
                                              trailing: Text(dateConvert(commentList[index]['created_at']),),
                                            )
                                        ),

                                      ),
                                    );
                                  },
                                ),
                              ),
                              Divider(),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10.0),
                                child: ListTile(
                                  // leading: showEmoji(),
                                  title: TextFormField(
                                    // focusNode: focusNode,
                                    maxLines: null,
                                    controller: commentController,
                                    decoration: InputDecoration(labelText: "Write a comment..."),
                                  ),
                                  trailing: IconButton(
                                    icon: Icon(Icons.send),
                                    onPressed: () {
                                      dailyComment();
                                      // storeComment();
                                    },
                                  ),
                                ),
                              ),                ]
                        ),
                      ),
                    ],
                  ),
                ) ;
              } else {
                return Container(
                    color: Colors.white,
                    child: Center(child: CircularProgressIndicator()));
              }
            })
        );
  }

  Future<String> getDaily() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    String role = pref.get('rule');
    // print(role);

    final response = await http.get(
        Uri.parse(InfixApi.getDailyDetail(int.parse(this.widget.id))));
    // print(Uri.parse(InfixApi.getDailyDetail(this.widget.id)));
    Map<String, dynamic> map = json.decode(response.body);

    setState(() {
      title = map['data']['daily']['title'];
      content = map['data']['daily']['content'];
      image = map['data']['daily']['image'];
      date = map['data']['daily']['date'];
      likeCount = map['data']['daily']['like_count'].toString();
      commentCount=map['data']['daily']['comment_count'].toString();
    });
    return "Success!";
  }

  Future<String> dailyLiked() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    // print(Uri.parse(InfixApi.dailyLike(int.parse(this.widget.id))));
    final response = await http.get(
        Uri.parse(InfixApi.dailyLike(int.parse(this.widget.id),int.parse(userId))));
    getDaily();
    return "Success!";
  }

  Future<String> dailyComment() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    // print(Uri.parse(InfixApi.dailyLike(int.parse(this.widget.id))));
    final response = await http.get(
        Uri.parse(InfixApi.dailyCommentStore(int.parse(this.widget.id),int.parse(userId),commentController.text)));
    // getDaily();
    commentController.text='';
    getDaily();
    getDailyComment();
    return "Success!";
  }

  Future<String> getDailyComment() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    String role = pref.get('rule');
    // print(role);

    final response = await http.get(
        Uri.parse(InfixApi.dailyCommentList(int.parse(this.widget.id))));
    // print(Uri.parse(InfixApi.dailyCommentList(int.parse(this.widget.id))));
    Map<String, dynamic> map = json.decode(response.body);

    setState(() {
      commentList = map['data']['commentList'];
    });
    print(commentList);
    return "Success!";
  }

  String dateConvert(String date){
    DateTime time=DateTime.parse(date);
    // String dateConvert = DateFormat("yyyy-MM-dd hh:mm:ss").format(time);
    String dateConvert = DateFormat("yyyy-MM-dd").format(time);
    return dateConvert.toString();

  }
}

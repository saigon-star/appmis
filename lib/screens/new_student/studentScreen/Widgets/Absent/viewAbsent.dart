import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/date_of_week_widget.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AbsentViewList extends StatefulWidget {
  final String date;

  const AbsentViewList({key, this.date}) : super(key: key);

  @override
  _AbsentViewListState createState() => _AbsentViewListState();
}

class _AbsentViewListState extends State<AbsentViewList> {
  List<dynamic> studentList;
  bool hasData = false;
  String date;

  @override
  void initState() {
    date = this.widget.date;
    // this.getStudentList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Container(
        color: Colors.white,
        child: FutureBuilder(
            future: getStudentList(),
            builder: (context, data) {
              if (hasData != false) {
                return ListView.builder(
                  itemCount: studentList.length,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0, left: 20.0, right: 20.0),
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Color(0xFF7dd3f7),
                                  borderRadius: BorderRadius.circular(40),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(
                                          0, 2), // changes position of shadow
                                    ),
                                  ]),
                              child: Center(
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 15.0, bottom: 15),
                                    child: Text('Attendance list'.toUpperCase(),
                                        style: TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff07509d)))),
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: ListTile(
                              leading: Text(''),
                              title: Text(''),
                              trailing: Text('Date: ' + date.toString()),
                            ),
                          ),
                          // The header
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: ListTile(
                              leading: Text(
                                '',
                                style: TextStyle(
                                    color: Color(0xFF144385),
                                    fontWeight: FontWeight.bold),
                              ),
                              title: Text('Name',
                                  style: TextStyle(
                                      color: Color(0xFF144385),
                                      fontWeight: FontWeight.bold)),
                              trailing: Text('Status',
                                  style: TextStyle(
                                      color: Color(0xFF144385),
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),

                          // The fist list item
                          _listItem(index)
                        ],
                      );
                    }
                    return _listItem(index);
                  },
                );
              } else {
                return Container(
                    color: Colors.white,
                    child: Center(child: CircularProgressIndicator()));
              }
            }),
      ),

    );
  }

  Widget _listItem(index) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: ListTile(
        leading: Text((index + 1).toString(), style: TextStyle(fontSize: 18)),
        title: Text(
          studentList[index]['full_name'].toString(),
          style: TextStyle(fontSize: 18),
        ),
        trailing: Text(studentList[index]['attendance_type'].toString(),
            style: TextStyle(fontSize: 18, color: Colors.purple)),
      ),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Colors.black26))),
    );
  }

  Future<String> getStudentList() async {
    final pref = await SharedPreferences.getInstance();
    String idUser = pref.get('id');

    DateTime now = DateTime.now().toUtc();
    final convertLocal = now.toLocal();
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    dynamic currentTime = dateFormat.format(convertLocal);
    if (date == '') {
      date = currentTime.toString();
    }

    // print(currentTime);
    // print(DateFormat.yMd().format(convertLocal));
    final response = await http
        .get(Uri.parse(InfixApi.getStudentAttendance(int.parse(idUser), date)));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      studentList = map["data"]["students"];
      hasData = true;
    });
    // print(studentList);

    return "Success!";
  }
}

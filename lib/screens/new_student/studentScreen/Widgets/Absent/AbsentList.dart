import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/date_of_week_widget.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AbsentList extends StatefulWidget {
  final String date;

  const AbsentList({key, this.date}) : super(key: key);

  @override
  _AbsentListState createState() => _AbsentListState();
}

class _AbsentListState extends State<AbsentList> {
  final items = List<String>.generate(10, (i) => "Item $i");
  List<dynamic> studentList;
  bool hasData = false;
  int val;

  int count = 0;

  @override
  void initState() {
    print(this.widget.date);
    this.getStudentList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(),
      body: absentRow(),
    );
  }

  Widget absentRow() {
    return Container(
        child: studentList != null && studentList.length > 0
            ? hasData != false
                ? ListView.builder(
                    itemCount: studentList.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(
                            top: 10.0, left: 20.0, right: 20.0),
                        child: Card(
                          color: Color(0xffebf6ff),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: ListTile(
                              contentPadding: EdgeInsets.only(left: 0.0),
                              isThreeLine: true,
                              leading: Container(
                                decoration: BoxDecoration(
                                  color: Color(0xFF7dd3f7),
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(30.0),
                                    bottomRight: Radius.circular(30.0),
                                  ),
                                ),
                                width: 35,
                                height: 30,
                                child: Center(
                                    child: Text(
                                  (index + 1).toString(),
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                )),
                              ),
                              title: Text(studentList[index]['full_name']),
                              subtitle: Column(
                                children: <Widget>[
                                  Align(
                                      alignment: Alignment.centerLeft,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: [
                                          ListTile(
                                            title: Text("Present"),
                                            leading: Radio(
                                              value: 1,
                                              groupValue: studentList[index]
                                                  ['admission_no'],
                                              onChanged: (value) {
                                                setState(() {
                                                  storeStudentSkill(
                                                      studentList[index]['id'],
                                                      this.widget.date,
                                                      'P');
                                                  print(value);
                                                  studentList[index]
                                                      ['admission_no'] = value;
                                                });
                                              },
                                              activeColor: Color(0xFF7dd3f7),
                                            ),
                                          ),
                                          ListTile(
                                            title: Text("Late"),
                                            leading: Radio(
                                              value: 2,
                                              groupValue: studentList[index]
                                                  ['admission_no'],
                                              onChanged: (value) {
                                                setState(() {
                                                  storeStudentSkill(
                                                      studentList[index]['id'],
                                                      this.widget.date,
                                                      'L');
                                                  print(value);
                                                  studentList[index]
                                                      ['admission_no'] = value;
                                                });
                                              },
                                              activeColor: Color(0xFF7dd3f7),
                                            ),
                                          ),
                                          ListTile(
                                            title: Text("Absent"),
                                            leading: Radio(
                                              value: 3,
                                              groupValue: studentList[index]
                                                  ['admission_no'],
                                              onChanged: (value) {
                                                setState(() {
                                                  storeStudentSkill(
                                                      studentList[index]['id'],
                                                      this.widget.date,
                                                      'A');
                                                  print(value);
                                                  studentList[index]
                                                      ['admission_no'] = value;
                                                });
                                              },
                                              activeColor: Color(0xFF7dd3f7),
                                            ),
                                          ),
                                          ListTile(
                                            title: Text("HalfDay"),
                                            leading: Radio(
                                              value: 4,
                                              groupValue: studentList[index]
                                                  ['admission_no'],
                                              onChanged: (value) {
                                                setState(() {
                                                  storeStudentSkill(
                                                      studentList[index]['id'],
                                                      this.widget.date,
                                                      'F');
                                                  print(value);
                                                  studentList[index]
                                                      ['admission_no'] = value;
                                                });
                                              },
                                              activeColor: Color(0xFF7dd3f7),
                                            ),
                                          ),
                                        ],
                                      ))
                                ],
                              )),
                        ),
                      );
                    })
                : Container(
                    color: Colors.white,
                    child: Center(child: CircularProgressIndicator()))
            : Container(
                child: Center(
                    child: Text(
                  'All students have been registered',
                  style: TextStyle(fontSize: 20, color: Color(0xFF144385)),
                )),
              ));
  }

  Future<String> getStudentList() async {
    final pref = await SharedPreferences.getInstance();
    String idUser = pref.get('id');
    DateTime now = DateTime.now();
    String date = widget.date;
    if (date == '') {
      date = now.toString();
    }
    final response = await http
        .get(Uri.parse(InfixApi.studentAbsentList(int.parse(idUser), date)));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      studentList = map["data"]["students"];
      hasData = true;
    });
    print(studentList);

    return "Success!";
  }

  Future<void> storeStudentSkill(int studentId, date, type) async {
    DateTime now = DateTime.now();
    if (date == '') {
      date = now.toString();
    }
    // print(date);
    final response =
        await http.get(Uri.parse(InfixApi.storeAbsent(studentId, date, type)));
  }
}

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/CardHeader.dart';
import 'package:infixedu/screens/new_student/studentScreen/ClinicWidget/General.dart';
import 'package:infixedu/screens/new_student/studentScreen/FeesAndPayment2.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class FeesAndPayment extends StatefulWidget {
  const FeesAndPayment({Key key}) : super(key: key);

  @override
  _FeesAndPaymentState createState() => _FeesAndPaymentState();
}

class _FeesAndPaymentState extends State<FeesAndPayment> {
  List<dynamic> unpaidPaymentList;
  List<dynamic> paidPaymentList;
  final unpaid = List<String>.generate(5, (i) => "Tuition fees");

  final paid = List<String>.generate(5, (i) => "Tuition fees");

  int _selected_fee = null;
  @override
  void initState() {
    this.getUnPaidPayment();
    this.getPaidPayment();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBarWidget(),
      body: DefaultTabController(
        length: 2,
        child: Column(
          children: [
            CardHeader(child: Text('FEES AND PAYMENT')),
            SizedBox(
              height: 20,
            ),
            TabBar(
                unselectedLabelColor: Color(0xFF7dd3f7),
                indicatorSize: TabBarIndicatorSize.label,
                labelColor: Colors.white,
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Color(0xFF9EDEFF)),
                tabs: [
                  Tab(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          border:
                              Border.all(color: Color(0xFF9EDEFF), width: 2)),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "UNPAID",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                  ),
                  Tab(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          border:
                              Border.all(color: Color(0xFF9EDEFF), width: 2)),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("PAID",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w700)),
                      ),
                    ),
                  ),
                ]),
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: TabBarView(children: [
                  ListView.builder(
                    itemCount: unpaidPaymentList!=null?unpaidPaymentList.length:0,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          selectfee(unpaidPaymentList[index]['id']);
                        },
                        child: Container(
                          padding: EdgeInsets.only(bottom: 10),
                          decoration: BoxDecoration(
                              border: Border(
                            bottom: BorderSide(
                                width: 3.0, color: Color(0xFF9EDEFF)),
                          )),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                paid[index],
                                style: TextStyle(
                                    fontSize: 20,
                                    color: _selected_fee == index
                                        ? Color(0xFF9EDEFF)
                                        : Colors.black),
                              ),
                              SizedBox(height: 10,),
                              Text("Payment Id: "+unpaidPaymentList[index]['id'].toString()),
                              SizedBox(height: 10.0,),
                              Text("Amount: " + NumberFormat.simpleCurrency(locale: 'vi').format(unpaidPaymentList[index]['amount']) ),
                              SizedBox(height: 10,),
                              Text("Discount: "+ unpaidPaymentList[index]['discount'].toString() + "%"),
                              SizedBox(height: 10,),
                              Text("Payment created: " + convertDate(unpaidPaymentList[index]['created_at']),style: TextStyle(
                                  fontSize: 18, color: Color(0xffe4087e)))
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  ListView.builder(
                    itemCount: paidPaymentList!=null?paidPaymentList.length:0,
                    itemBuilder: (context, index) {
                      return Container(
                        padding: EdgeInsets.only(bottom: 10),
                        decoration: BoxDecoration(
                            border: Border(
                          bottom:
                              BorderSide(width: 3.0, color: Color(0xFF9EDEFF)),
                        )),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                unpaid[index],
                                style: TextStyle(
                                    fontSize: 20, color: Colors.black),
                              ),
                              SizedBox(height: 10.0,),
                              Text("Payment Id: "+ paidPaymentList[index]['id'].toString()),
                              SizedBox(height: 10.0,),
                              Text("Amount: " + NumberFormat.simpleCurrency(locale: 'vi').format(int.parse(paidPaymentList[index]['amount'])) ),
                              SizedBox(height: 10.0,),
                              Text('Paid',
                                  style: TextStyle(
                                      fontSize: 18, color: Color(0xFF9EDEFF))),
                              SizedBox(height: 10.0,),
                              Text('Payment created: '+ convertDate(paidPaymentList[index]['created_at']),
                                  style: TextStyle(
                                      fontSize: 18, color: Color(0xffe4087e)))
                            ]),
                      );
                    },
                  )
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void selectfee(int index) {
    setState(() {
      _selected_fee = index;
    });
    Timer(Duration(milliseconds: 300), () {
      pushNewScreen(
        context,
        screen: FeesAndPayment2(id: index),
        withNavBar: false, // OPTIONAL VALUE. True by default.
        pageTransitionAnimation: PageTransitionAnimation.cupertino,
      );
      setState(() {
        _selected_fee = null;
      });
    });
  }

  Future<String> getUnPaidPayment() async {
    final pref = await SharedPreferences.getInstance();
    String id = pref.get('id');


    final response =
    await http.get(Uri.parse(InfixApi.getStudentPaymentUnpaid(int.parse(id))));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      unpaidPaymentList = map["data"]["feeList"];
    });
    print(unpaidPaymentList);
    return "Success!";
  }
  Future<String> getPaidPayment() async {
    final pref = await SharedPreferences.getInstance();
    String id = pref.get('id');


    final response =
    await http.get(Uri.parse(InfixApi.getStudentPaymentPaid(int.parse(id))));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      paidPaymentList = map["data"]["feeList"];
    });
    print(paidPaymentList);
    return "Success!";
  }
  String convertDate(String date){
    DateTime parseDate = new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);
    var inputDate = DateTime.parse(parseDate.toString());
    final convertLocal = inputDate.toLocal();
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    dynamic formatDate = dateFormat.format(convertLocal);
    return formatDate;
  }
}

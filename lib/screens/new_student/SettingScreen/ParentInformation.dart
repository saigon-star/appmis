import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarMainScreen.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/CardHeader.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class ParentInformation extends StatefulWidget {
  const ParentInformation({key}) : super(key: key);

  @override
  _ParentInformationState createState() => _ParentInformationState();
}

class _ParentInformationState extends State<ParentInformation> {
  String fatherName;
  String fatherNationality;
  String fatherCompany;
  String fatherWorkAddress;
  String fatherPhone;
  String fatherEmail;
  String fatherFirstLanguage;
  String fatherEnglishLevel;
  String studentPhoto;
  String studentName;
  @override
  void initState() {
    getParentInfo();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.4,
                color: Color(0xFF9EDEFF),
              ),
            ],
          ),
          Positioned(
            top: 50,
            left: 50,
            right: 50,
            child: Column(
              children: [
                CircleAvatar(
                  radius: 55.0,
                  backgroundImage:
                  studentPhoto!=null?NetworkImage('https://sgstar.asia/'+ studentPhoto.toString()):AssetImage('assets/images/icons/student1.png'),
                  backgroundColor: Colors.white,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  studentName!=null?studentName:'',
                  style: TextStyle(
                      color: Color(0xFF144385), fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          Positioned(
            top: 230,
            left: 20,
            right: 20,
            child: Container(
              //width: MediaQuery.of(context).size.width*1,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  border: Border.all(
                      color: Color(0xFF9EDEFF), // Set border color
                      width: 1.0),
                  color: Colors.white),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 25.0),
                    child: Text(
                      'Parent information'.toUpperCase(),
                      style: TextStyle(
                          fontSize: 15,
                          color: Color(0xFF144385),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(25.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(width: 1.0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [Text("Full name:"), Text(fatherName!=null?fatherName:'')],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(25.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(width: 1.0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [Text("Phone number"), Text(fatherPhone!=null?fatherPhone:"")],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(25.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(width: 1.0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [Text("Email:"), Text(fatherEmail!=null?fatherEmail:'')],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(25.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(width: 1.0),
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [Text("Relationship:"), Text("Parent")],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> getParentInfo() async {
    final pref = await SharedPreferences.getInstance();
    String id = pref.get('StudentId');
    final response = await http.get(Uri.parse(InfixApi.getStudentInfo(int.parse(id))));
    var jsonData = json.decode(response.body);
    print(jsonData['data']['parent_detail']);
    if (mounted) {
      setState(() {
        //Father
        fatherName = jsonData['data']['parent_detail']['fathers_name'];
        fatherNationality =
            jsonData['data']['parent_detail']['fathers_nationality'];
        fatherCompany = jsonData['data']['parent_detail']['fathers_company'];
        fatherWorkAddress =
            jsonData['data']['parent_detail']['fathers_work_address'];
        fatherPhone =
            jsonData['data']['parent_detail']['fathers_mobile'].toString();
        fatherEmail = jsonData['data']['parent_detail']['fathers_email'];
        fatherFirstLanguage =
            jsonData['data']['parent_detail']['fathers_first_language'];
        fatherEnglishLevel =
            jsonData['data']['parent_detail']['fathers_language_level'];

        studentPhoto=jsonData['data']['student_detail']['student_photo'];
        studentName=jsonData['data']['student_detail']['full_name'];
      });
    }
  }
}

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/BusDropDown.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/bus_history.dart';
import 'package:infixedu/screens/new_student/BusScreen/GuardianRegister.dart';
import 'package:infixedu/screens/new_student/BusScreen/RegisterBus.dart';
import 'package:infixedu/screens/new_student/BusScreen/RouteTracking.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarMainScreen.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/custom_dropdown.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class EnrolledStudentList extends StatefulWidget {
  final int idRoute;

  const EnrolledStudentList({Key key, this.idRoute}) : super(key: key);

  @override
  _EnrolledStudentListState createState() => _EnrolledStudentListState();
}

class _EnrolledStudentListState extends State<EnrolledStudentList> {
  List<dynamic> studentList;
  bool hasData=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background/BG_BUS-01.png"),
            fit: BoxFit.fill,
          ),
        ),
        child: FutureBuilder(
            future: getStudentList(),
            builder: (context, data) {
              if (hasData != false) {
                return ListView.builder(
                  itemCount: studentList.length,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0, left: 20.0, right: 20.0),
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Color(0xFF7dd3f7),
                                  borderRadius: BorderRadius.circular(40),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 1,
                                      blurRadius: 2,
                                      offset: Offset(
                                          0, 2), // changes position of shadow
                                    ),
                                  ]),
                              child: Center(
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 15.0, bottom: 15),
                                    child: Text('Attendance list'.toUpperCase(),
                                        style: TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff07509d)))),
                              ),
                            ),
                          ),
                          // The header
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: ListTile(
                              leading: Text(
                                '',
                                style: TextStyle(
                                    color: Color(0xFF144385),
                                    fontWeight: FontWeight.bold),
                              ),
                              title: Text('Name',
                                  style: TextStyle(
                                      color: Color(0xFF144385),
                                      fontWeight: FontWeight.bold)),
                              trailing: Text('Status',
                                  style: TextStyle(
                                      color: Color(0xFF144385),
                                      fontWeight: FontWeight.bold)),
                            ),
                          ),

                          // The fist list item
                          _listItem(index)
                        ],
                      );
                    }
                    return _listItem(index);
                  },
                );
              } else {
                return Container(
                    color: Colors.white,
                    child: Center(child: CircularProgressIndicator()));
              }
            }),
      ),
    );
  }

  Widget _listItem(index) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: ListTile(
        leading: Text((index + 1).toString(), style: TextStyle(fontSize: 18)),
        title: Text(
          studentList[index]['full_name'].toString(),
          style: TextStyle(fontSize: 18),
        ),
        trailing: Text(studentList[index]['attendance_type'].toString(),
            style: TextStyle(fontSize: 18, color: Colors.purple)),
      ),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Colors.black26))),
    );
  }

  Future<String> getStudentList() async {
    String date;
    final pref = await SharedPreferences.getInstance();
    String idUser = pref.get('id');

    DateTime now = DateTime.now().toUtc();
    final convertLocal = now.toLocal();
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    dynamic currentTime = dateFormat.format(convertLocal);
    date = currentTime.toString();

    // print(currentTime);
    // print(DateFormat.yMd().format(convertLocal));
    final response = await http
        .get(Uri.parse(InfixApi.getEnrollStudentList(this.widget.idRoute, date)));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      studentList = map["data"]["attendanceBus"];
      hasData = true;
    });
    // print(studentList);

    return "Success!";
  }
}
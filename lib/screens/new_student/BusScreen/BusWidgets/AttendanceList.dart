import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/BusDropDown.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/bus_history.dart';
import 'package:infixedu/screens/new_student/BusScreen/GuardianRegister.dart';
import 'package:infixedu/screens/new_student/BusScreen/RegisterBus.dart';
import 'package:infixedu/screens/new_student/BusScreen/RouteTracking.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarMainScreen.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/custom_dropdown.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AttendanceListBus extends StatefulWidget {
  final int idRoute;

  const AttendanceListBus({Key key, this.idRoute}) : super(key: key);

  @override
  _AttendanceListBusState createState() => _AttendanceListBusState();
}

class _AttendanceListBusState extends State<AttendanceListBus> {
  bool _value = false;
  int val = 0;
  String date;
  List<dynamic> studentList;
  bool hasData = false;

  @override
  void initState() {
    this.getStudentList();
    // print(this.widget.idRoute);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(),
        body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background/BG_BUS-01.png"),
              fit: BoxFit.fill,
            ),
          ),
          child: studentList!=null&&studentList.length>0 ?Container(
            child: ListView.builder(
                itemCount: studentList != null ? studentList.length : 0,
                itemBuilder: (context, index) {
                  return ExpansionTile(
                    title: Text(
                      studentList[index]['full_name'],
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    ),
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Container(
                              // width: 250,
                              height: 50,
                              child: ListTile(
                                title: Text("Present"),
                                leading: Radio(
                                  value: 1,
                                  groupValue: studentList[index]
                                  ['admission_no'],
                                  onChanged: (value) {
                                    setState(() {
                                      this.storeStudentAttendance(
                                          studentList[index]['id'], 'P');
                                      studentList[index]['admission_no'] =
                                          value;
                                    });
                                  },
                                  activeColor: Colors.green,
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 50,
                              child: ListTile(
                                title: Text("Absent"),
                                leading: Radio(
                                  value: 2,
                                  groupValue: studentList[index]
                                  ['admission_no'],
                                  onChanged: (value) {
                                    setState(() {
                                      this.storeStudentAttendance(
                                          studentList[index]['id'], 'A');
                                      studentList[index]['admission_no'] =
                                          value;
                                    });
                                  },
                                  activeColor: Colors.green,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  );
                }),
          ) :Container(
            child: Center(
              child: Text('You had attendance all student'),
            ),
          ),
        ));
  }

  Future<String> getStudentList() async {
    DateTime now = DateTime.now().toUtc();
    final convertLocal = now.toLocal();
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    dynamic currentTime = dateFormat.format(convertLocal);
    date = currentTime.toString();
    final response = await http
        .get(Uri.parse(InfixApi.getRouteStudentList(this.widget.idRoute)));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      studentList = map["data"]["routeList"];
      hasData = true;
    });
    print(studentList);
    return "Success!";
  }

  Future<void> storeStudentAttendance(int studentId, type) async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    DateTime now = DateTime.now();
    final response = await http.get(Uri.parse(
        InfixApi.storeStudentBusAttendance(
            studentId, now.toString(), type, userId,this.widget.idRoute)));
  }
}

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/BusDropDown.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/bus_history.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/enrolledStudent.dart';
import 'package:infixedu/screens/new_student/BusScreen/GuardianRegister.dart';
import 'package:infixedu/screens/new_student/BusScreen/RegisterBus.dart';
import 'package:infixedu/screens/new_student/BusScreen/RouteTracking.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarMainScreen.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/custom_dropdown.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:intl/intl.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'BusWidgets/AttendanceList.dart';

class StudentListBus extends StatefulWidget {
  const StudentListBus({Key key}) : super(key: key);

  @override
  _StudentListBusState createState() => _StudentListBusState();
}

class _StudentListBusState extends State<StudentListBus> {
  List<dynamic> leaveTypeList;
  List<dynamic> routeList;
  bool hasData = false;
  String dropdownValue;
  String date;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background/BG_BUS-01.png"),
              fit: BoxFit.fill,
            ),
          ),
          child: FutureBuilder(
            future: getRouteList(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 15.0),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          child: Text(
                            "Select Bus Route",
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff07509d)),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Container(
                        width: double.infinity,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border:
                                Border.all(color: Color(0xff7cd3f7), width: 2)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: DropdownButtonHideUnderline(
                            child: new DropdownButton(
                              value: dropdownValue,
                              icon: const Icon(Icons.arrow_downward),
                              iconSize: 10,
                              elevation: 12,
                              onChanged: (String newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                  print(dropdownValue);
                                });
                              },
                              items: routeList.map((item) {
                                return DropdownMenuItem<String>(
                                  value: item['id'].toString(),
                                  child: Text(
                                    item['title'],
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                              hint: Text(
                                "Select Route",
                                style: TextStyle(color: Colors.grey),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Container(
                        child: Text(
                          "Today is: " + date.toString(),
                          style: TextStyle(
                              fontSize: 13.0,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff07509d)),
                        ),
                      ),
                    ),
                    ElevatedButton(
                      child: Text("Attendance"),
                      onPressed: () {
                        if (dropdownValue != null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AttendanceListBus(
                                    idRoute: int.parse(dropdownValue)),
                              ));
                        } else
                          Fluttertoast.showToast(
                              msg: "Please select date",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.TOP,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Color(0xff7cd3f7),
                              textColor: Colors.white,
                              fontSize: 16.0);
                      },
                    ),
                    ElevatedButton(
                      child: Text("List Of Enrolled Students"),
                      onPressed: () {
                        if (dropdownValue != null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AttendanceListBus(
                                    idRoute: int.parse(dropdownValue)),
                              ));
                        } else
                          Fluttertoast.showToast(
                              msg: "Please select date",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.TOP,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Color(0xff7cd3f7),
                              textColor: Colors.white,
                              fontSize: 16.0);
                      },
                    ),
                  ],
                );
              } else
                return Container(
                    color: Colors.white,
                    child: Center(child: CircularProgressIndicator()));
            },
          )),
    );
  }

  Future<String> getLeaveTypeList() async {
    final pref = await SharedPreferences.getInstance();
    String classId = pref.get('class_id');

    final response = await http.get(Uri.parse(InfixApi.schoolLeaveType(2)));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      leaveTypeList = map["data"]["leaveType"];
      hasData = true;
    });
    // print(leaveTypeList);
    return "Success!";
  }

  Future<String> getRouteList() async {
    DateTime now = DateTime.now().toUtc();
    final convertLocal = now.toLocal();
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    dynamic currentTime = dateFormat.format(convertLocal);
    date = currentTime.toString();

    final response = await http.get(Uri.parse(InfixApi.getRouteList()));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      routeList = map["data"]["routeList"];
      hasData = true;
    });
    // print(leaveTypeList);
    return "Success!";
  }
}

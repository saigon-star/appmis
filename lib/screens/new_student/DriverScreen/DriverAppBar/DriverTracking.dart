import 'dart:convert';
import 'dart:typed_data';
import 'package:geolocator/geolocator.dart' as geo;
import 'package:dropdown_below/dropdown_below.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';
import 'package:infixedu/screens/new_student/studentScreen/StudentInfoWidget/StudentInfoADM.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:location/location.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:http/http.dart' as http;

class RouteTrackingDriver extends StatefulWidget {
  const RouteTrackingDriver({Key key}) : super(key: key);

  @override
  _RouteTrackingDriverState createState() => _RouteTrackingDriverState();
}

class _RouteTrackingDriverState extends State<RouteTrackingDriver> {
  List<dynamic> busStation;
  List<dynamic> listStudentBus;
  StreamSubscription _locationSubcription;
  double latitudeNow = 10.76619;
  double longtitudeNow = 106.7610729;
  Marker marker; //khởi tạo marker
  GoogleMapController _controller; // khởi tạo controller
  double bus_latitude; // khởi tạo kinh độ trống
  double bus_longtitude; // khởi tạo vĩ độ trống
  double route_latitude =
  10.766; //khởi tạo kinh độ của trường saigonstar, Khi app chạy, camera sẽ tự lia đến vị trí này
  double route_longtitude =
  106.7611129; //khởi tạo vĩ độ của trường saigonstar, Khi app chạy, camera sẽ tự lia đến vị trí này
  double rotate = 0; // khởi tạo đôj xoay của camera mặc định là 0 độ
  CameraPosition _currentPosition;
  Timer timer;
  String busId;
  int _selectedBus; //khởi tạo xe bus đang được chọn
  List _listBus; //khởi tạo danh sách tất cả xe bus.
  String btn1;
  String btn2;
  int routeId;
  String kAppId = 'c66363cb-07d7-4c99-bc36-8fd788769c04';
  // static const LatLng schoolLocation = const LatLng(10.76619, 106.7610729);

  Set<Polyline> polyList = {};
  Set<Polyline> polylineSet = Set<Polyline>();
  List<LatLng> polyLineCoor = [];
  PolylinePoints polyLinePoints;

  int studentBusId;

  Future<Uint8List> getMarker() async {
    // Method lấy icon marker phân giải nó sang định dạng mà google map flutter có thể đọc được.
    ByteData byData = await DefaultAssetBundle.of(context)
        .load("assets/images/icons/bus_icon.png");
    return byData.buffer.asUint8List();
  }

  void sendNotificationBus(int routeId, int stationId) async {
    final response =
    await http.get(Uri.parse(InfixApi.notificationBus(routeId, stationId)));
    print('upload Notification ok');
  }

  void updateMarkerAndCircle(Uint8List imageData) {
    // Method cập nhật lại vị trí của marker với kinh độ ,vĩ độ và rotation thay đổi. Ví dụ khi user xoay map hình vòng tròn thì marker cũng phải xoay theo
    LatLng latlng = LatLng(bus_latitude, bus_longtitude);
    this.setState(() {
      marker = Marker(
          markerId: MarkerId("home"),
          position: latlng,
          rotation: 0 + rotate,
          // thiết lập xoay marker ở đây
          draggable: false,
          zIndex: 2,
          flat: true,
          anchor: Offset(0.5, 0.5),
          icon: BitmapDescriptor.fromBytes(imageData));
    });
  }

  void getTeacherVehicles() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    final response =
    await http.get(Uri.parse(InfixApi.getDriverVehicle(int.parse(userId))));
    var jsonData = jsonDecode(response.body);
    print('id transport:' + jsonData['data']['route']['vehicle_id'].toString());
    if (mounted) {
    setState(() {
      studentBusId = jsonData['data']['route']['vehicle_id'];
      routeId = jsonData['data']['route']['route_id'];
    });}
    // createdPolyline();
    // setPolyLine();
    print('Vehicle Id: ' + jsonData['data']['route']['vehicle_id'].toString());
    print('La Now:' + latitudeNow.toString());
    print('Long now: ' + longtitudeNow.toString());
    updateLocation(jsonData['data']['route']['vehicle_id'],
        latitudeNow, longtitudeNow);
  }

  void studentBusList() async{
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    print('Bus User: ' + userId.toString());
    final response = await http.get(Uri.parse(InfixApi.studentBusList(int.parse(userId))));

    print(Uri.parse(InfixApi.studentBusList(int.parse(userId))));

    // if (response.statusCode == 200) {
    //   print('abc');
    // }
    var jsonData = jsonDecode(response.body);
    print(jsonData['data']);
    if(mounted){
      setState(() {
        listStudentBus = jsonData['data']['studentList'];
      });
    }

    print(listStudentBus);
  }
  void getListVehicles() async {
    // Method lấy danh sách các phương tiện từ server
    final response = await http.get(Uri.parse(InfixApi.vehiclesList()));
    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);
      print(jsonData['data']);
      setState(() {
        _listBus = jsonData['data'];
      });
    } else {
      throw Exception('failed to load');
    }
  }

  void getStudentBus() async {
    final pref = await SharedPreferences.getInstance();
    studentBusId = pref.get('busId');
    if (studentBusId != "null") {
      getVehiclesLocation(studentBusId.toString());
      moveToRoute();
      print("upload location");
    } else
      print("teacher assign");
  }

  void getBusStation() async {
    final pref = await SharedPreferences.getInstance();
    String userId = pref.get('id');
    final response =
    await http.get(Uri.parse(InfixApi.getBusStation(int.parse(userId))));
    var jsonData = jsonDecode(response.body);
    setState(() {
      busStation = jsonData['data']['route'];
    });
    print(busStation);
  }

  void getVehiclesLocation(id) async {
    // Method lấy vị trí theo id phương tiện từ server
    final response = await http.get(Uri.parse(InfixApi.vehiclesLocation(id)));
    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);
      if (mounted) {
      setState(() {
        //set dữ liệu mới cho 2 tham số bus_latitude,bus_longtitude đã khởi tạo phía trên
        bus_latitude = jsonData['data']['latitude'];
        bus_longtitude = jsonData['data']['longitude'];
      });}
      getCurrentLocation(); // sau khi có dữ liệu kinh, vĩ độ mới thì gọi hàm này để set lại vị trí marker
      print("get bus location");
    } else {
      throw Exception('failed to load');
    }
  }

  getUserLocation() async {
    final positionGeo = await geo.Geolocator.getCurrentPosition(
        desiredAccuracy: geo.LocationAccuracy.high);
    setState(() {
      latitudeNow = positionGeo.latitude.toDouble();
      longtitudeNow = positionGeo.longitude.toDouble();
    });
  }

  void getCurrentLocation() async {
    try {
      //Method kiểm tra xem người dùng có đồng ý chia sẻ vị trí với thiết bị không nếu có thì gọi tiếp hàm update marker nếu không thì báo lỗi
      Uint8List imageData = await getMarker();
      updateMarkerAndCircle(imageData);
      if (_locationSubcription != null) {
        _locationSubcription.cancel();
      }
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        debugPrint("Permission Denied");
      }
    }
  }

  void moveToRoute() async {
    // Method này dùng để lia camera đến vị trí của phương tiện
    if (_controller != null) {
      _controller.animateCamera(CameraUpdate.newCameraPosition(
          new CameraPosition(
              bearing: 0,
              target: LatLng(bus_latitude, bus_longtitude),
              tilt: 0,
              zoom: 14.5)));
    }
  }

  @override
  void dispose() {
    // huỷ các hàm chạy ngầm khi thoát
    if (_locationSubcription != null) {
      _locationSubcription.cancel();
    }
    getUserLocation();
    timer?.cancel();
    super.dispose();
  }

  Future<void> updateLocation(int $id, double latitude,
      double longtitude) async {
    getUserLocation();

    final response = await http
        .get(Uri.parse(InfixApi.updateLocationBus($id, latitude, longtitude)));
    print('bus id: ' + $id.toString());
    getVehiclesLocation($id);
    // moveToRoute();
    print("Success!");
  }

  @override
  void initState() {
    studentBusList();
    getTokenId();
    polyLinePoints = PolylinePoints();
    getCurrentLocation();
    getBusStation();
    getUserLocation(); //lấy thông tin vị trí của user khi mở App - Khoa
    //Ngay khi screen được khởi tạo xong
    _currentPosition = CameraPosition(
        target: LatLng(latitudeNow, longtitudeNow),
        zoom: 14); // lia camera đến vị trí tương ứng
    getListVehicles(); // và gọi method lấy danh sách các phương tiện

    // timer =
        Timer.periodic(Duration(seconds: 5), (timer) => getTeacherVehicles());
    //Cứ mỗi 5 giây, gửi yêu cầu lên server để lấy về vị trí của phương tiện đã chọn

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: SlidingUpPanel(
        minHeight: 50,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        panel: Container(
          child: slideUp(),
        ),
        body: GoogleMap(
          polylines: polylineSet,
          mapType: MapType.normal,
          initialCameraPosition: _currentPosition,
          markers: Set.of((marker != null) ? [marker] : []),
          onMapCreated: (GoogleMapController controller) {
            _controller = controller;
            setPolyLine();
          },
          onCameraMove: (CameraPosition _currentCamPosition) async {
            setState(() {
              rotate = _currentCamPosition.bearing;
            });
            Uint8List imageData = await getMarker();
            updateMarkerAndCircle(imageData);
          },
        ),
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.only(top: 100.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: FloatingActionButton(
                heroTag: btn1,
                child: Icon(Icons.location_searching),
                onPressed: () {
                  getVehiclesLocation(studentBusId);
                  moveToRoute();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget slideUp() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 50,
          child: Center(
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Color(0xff07509d)),
              height: 10,
              width: 60,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(top: 20.0),
            child: Container(
              height: 50,
              child: ListView.builder(
                itemCount: busStation != null ? busStation.length : 0,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          child: Text(busStation[index]['bus_stop_name']),
                        ),
                        Container(
                          child: TextButton(
                            child: Text('Send Notification'),
                            // icon: Icon(Icons.send),
                            onPressed: () {
                              sendNotification('Bus is coming to: ' + busStation[index]['bus_stop_name'],'The bus is coming near ' + busStation[index]['bus_stop_name'] + ' please prepare for your student ready before the bus come as soon as possible');
                              // sendNotificationBus(
                              //     int.parse(busStation[index]['route_id']),
                              //     (busStation[index]['id']));
                            },

                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  void getTokenId() async{
    OneSignal.shared.getDeviceState().then((deviceState) =>
        print("OneSignal: device state: ${deviceState.userId}"));
    // OneSignal.shared.getDeviceState()
  }

  Future<Response> sendNotification(String heading, String contents) async{
    var arr=[];

    int test=0;
    print(listStudentBus);
    listStudentBus.forEach((element) {
      arr.add(element['appId']);
    });

    print(arr);
    OneSignal.shared.getDeviceState().then((deviceState) =>
        print("OneSignal: device state: ${deviceState.userId}"));
    final response =
     await post(
      Uri.parse('https://onesignal.com/api/v1/notifications'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>
      {
        "app_id": kAppId,//kAppId is the App Id that one get from the OneSignal When the application is registered.

        "include_player_ids": arr,

        // android_accent_color reprsent the color of the heading text in the notifiction
        "android_accent_color":"7cd3f7",

        "small_icon":"splash",

        "large_icon":"https://sgstar.edu.vn/public/frontEnd/assets/img/logo/harvard-logo.png",

        "headings": {"en": heading},

        "contents": {"en": contents},


      }),
    );

    print(response.body);
  }

  void setPolyLine() async {
    polyLineCoor.clear();
    PolylineResult result = await polyLinePoints.getRouteBetweenCoordinates(
        'AIzaSyBCXotjcvMjEZurGw3Sy6T6FFsPNwWD7y0',
        PointLatLng(latitudeNow, longtitudeNow),
        PointLatLng(10.76619, 106.7610729));
    print(result.status);
    if (result.status == 'OK') {
      result.points.forEach((PointLatLng point) {
        polyLineCoor.add(LatLng(point.latitude, point.longitude));
      });

      setState(() {
        polylineSet.add(Polyline(
          polylineId: PolylineId('1'),
          color: Colors.red,
          width: 3,
          points: polyLineCoor,
          // startCap: Cap.roundCap,
          // endCap: Cap.buttCap,
          // patterns: [PatternItem.dash(20),PatternItem.gap(10)]),
        ));
      });
    }
  }
}

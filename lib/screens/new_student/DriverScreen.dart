import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusScreen.dart';
import 'package:infixedu/screens/new_student/BusScreen/BusWidgets/RouteTrackingTeacher.dart';
import 'package:infixedu/screens/new_student/ChatScreen/ChatScreen.dart';
import 'package:infixedu/screens/new_student/DriverScreen/DriverAppBar/DriverAppBar.dart';
import 'package:infixedu/screens/new_student/ELearningScreen/ElearningScreen.dart';
import 'package:infixedu/screens/new_student/HomeScreen/HomeScreen.dart';
import 'package:infixedu/screens/new_student/SettingScreen/SettingsScreen.dart';
import 'package:infixedu/screens/new_student/studentScreen/StudentScreen.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'DriverScreen/DriverAppBar/DriverTracking.dart';
import 'DriverScreen/DriverAppBar/SettingDriver.dart';

class DriverScreen extends StatefulWidget {
  var _titles;
  var _images;

  DriverScreen(this._titles, this._images);

  @override
  _DriverScreenState createState() => _DriverScreenState();
}

class _DriverScreenState extends State<DriverScreen> {
  int role;
  bool isChecked = false;

  @override
  void initState() {
    this.getRole();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return Scaffold(
      appBar: DriverAppBar(),
      body: Container(
        child: Container(
          child: GridView.count(
            childAspectRatio: (100 / 165),
            primary: true,
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 20,
            mainAxisSpacing: 0,
            crossAxisCount: shortestSide < 600 ? 3 : 6,
            children: <Widget>[
              Container(
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          _navigate(1);
                        },
                        child: Container(
                          height: 100,
                          padding: const EdgeInsets.all(0),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image:AssetImage('assets/images/icons/route.jpg')),
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              width: 2,
                              color: const Color(0xFF7dd3f7),
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Text(
                              'route tracking'.toUpperCase(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  color: Color(0xff07509d)),
                              textAlign: TextAlign.center,
                            ),
                          ))
                    ],
                  )),
              Container(
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          _navigate(2);
                        },
                        child: Container(
                          height: 100,
                          padding: const EdgeInsets.all(0),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image:AssetImage('assets/images/icons/settingColor.jpg')),
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              width: 2,
                              color: const Color(0xFF7dd3f7),
                            ),
                          ),
                        ),
                      ),
                      Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Text(
                              'Setting'.toUpperCase(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  color: Color(0xff07509d)),
                              textAlign: TextAlign.center,
                            ),
                          ))
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }

  void _navigate(int num) {
    switch (num) {
      case 1:
        pushNewScreen(
          context,
          screen: RouteTrackingDriver(),
          withNavBar: true, // OPTIONAL VALUE. True by default.
          pageTransitionAnimation: PageTransitionAnimation.cupertino,
        );
        break;
      case 2:
        pushNewScreen(
          context,
          screen: SettingDriverScreen(),
          withNavBar: true, // OPTIONAL VALUE. True by default.
          pageTransitionAnimation: PageTransitionAnimation.cupertino,
        );
        break;
      default:
        break;
    }
  }

  int getRole() {
    Utils.getStringValue('rule').then((value) {
      setState(() {
        role = int.parse(value);
      });
    });
    return role;
  }
}
import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';

import 'package:infixedu/screens/new_student/HomeScreen/Widgets/NewsComment.dart';
import 'package:infixedu/screens/new_student/HomeScreen/Widgets/NewsContent.dart';
import 'package:infixedu/screens/new_student/HomeScreen/Widgets/blockUser.dart';
import 'package:infixedu/screens/new_student/HomeScreen/Widgets/editNews.dart';
import 'package:infixedu/screens/new_student/studentScreen/studentPostScreen.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:share/share.dart';
import 'dart:async';

import 'AddNoteBoard.dart';

class NoteBoard extends StatefulWidget {
  const NoteBoard({key}) : super(key: key);

  @override
  _NoteBoardState createState() => _NoteBoardState();
}

class _NoteBoardState extends State<NoteBoard> {
  final items = List<String>.generate(5, (i) => "Item $i");
  bool hasData = false;
  List<dynamic> listNoteBoard;

  @override
  void initState() {
    this.getNoteBoard();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Container(
        color: Colors.white,
        child: FutureBuilder(
          future: this.getNoteBoard(),
          builder: (context,data){
            if(data.hasData){
              return ListView.builder(
                itemCount: listNoteBoard.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Card(
                      color: Color(0xffebf6ff),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: ListTile(
                          contentPadding: EdgeInsets.only(left: 0.0),
                          isThreeLine: true,
                          leading: Container(
                            decoration: BoxDecoration(
                              color: Color(0xFF7dd3f7),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30.0),
                                bottomRight: Radius.circular(30.0),
                              ),
                            ),
                            width: 35,
                            height: 30,
                            child: Center(
                                child: Text(
                                  (index + 1).toString(),
                                  style: TextStyle(fontSize: 18, color: Colors.white),
                                )),
                          ),
                          title: Text(listNoteBoard[index]['title']),
                          subtitle: Column(
                            children: <Widget>[
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(listNoteBoard[index]['created_at'])),
                              Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(listNoteBoard[index]['content']))
                            ],
                          )),
                    ),
                  );
                },
              );
            }
            else{
              return Container(
                  color: Colors.white,
                  child: Center(child: CircularProgressIndicator()));
            }
          },
        ),
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.only(bottom: 15.0),
        child: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const AddNoteBoard()),
            );
            // Add your onPressed code here!
          },
          child: const Icon(Icons.post_add_outlined),
          backgroundColor: Color(0xFF7dd3f7),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Future<String> getNoteBoard() async {
    final pref = await SharedPreferences.getInstance();
    String classId = pref.get('class_id');
    String userId = pref.get('id');

    final response =
        await http.get(Uri.parse(InfixApi.getNoteBoard(int.parse(userId))));
    Map<String, dynamic> map = json.decode(response.body);
    setState(() {
      listNoteBoard = map["data"]["noteBoard"];
      hasData = true;
    });
    // print(listNoteBoard);
    return "Success!";
  }
}

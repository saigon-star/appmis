import 'dart:convert';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_summernote/flutter_summernote.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:infixedu/screens/new_student/CommonWidgets/AppBarWidget.dart';

import 'package:infixedu/screens/new_student/HomeScreen/Widgets/NewsComment.dart';
import 'package:infixedu/screens/new_student/HomeScreen/Widgets/NewsContent.dart';
import 'package:infixedu/screens/new_student/HomeScreen/Widgets/blockUser.dart';
import 'package:infixedu/screens/new_student/HomeScreen/Widgets/editNews.dart';
import 'package:infixedu/screens/new_student/studentScreen/studentPostScreen.dart';
import 'package:infixedu/utils/Utils.dart';
import 'package:infixedu/utils/apis/Apis.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:share/share.dart';
import 'dart:async';

class AddNoteBoard extends StatefulWidget {
  const AddNoteBoard({key}) : super(key: key);

  @override
  _AddNoteBoardState createState() => _AddNoteBoardState();
}

class _AddNoteBoardState extends State<AddNoteBoard> {
  GlobalKey<FlutterSummernoteState> _keyEditor = GlobalKey();
  TextEditingController _titleController = TextEditingController();
  var bodyContent;
  bool isSending = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(),
      body: Container(
        padding: EdgeInsets.only(bottom: 20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                child: Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.075,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                    color: Color(0xFF9EDEFF),
                    borderRadius: BorderRadius.circular(
                      45.0,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      'add new note'.toUpperCase(),
                      style: TextStyle(
                          fontSize: 25,
                          color: Color(0xFF144385),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: TextField(
                  maxLines: null,
                  controller: _titleController,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xff7cd3f7), width: 2.0),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: Color(0xff7cd3f7), width: 2.0),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    label: Text(
                      'Title',
                      style: TextStyle(color: Color(0xFF9EDEFF)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 20, 5),
                child: Container(
                    child: FlutterSummernote(
                      height: 500,
                      value: '',
                      key: _keyEditor,
                      hasAttachment: false,
                      customToolbar: """
                          [
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['insert', ['link', 'table', 'hr']]
                          ]
                        """,
                    ),
                    margin: MediaQuery.of(context).viewInsets),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: Container(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      textStyle: TextStyle(fontSize: 24),
                      maximumSize: Size.fromHeight(80),
                      shape: StadiumBorder(),
                    ),
                    onPressed: () async {
                      bodyContent = await _keyEditor.currentState.getText();
                      bodyContent = await _keyEditor.currentState.getText();
                      print(bodyContent);
                     addNote();
                      if (isSending) return;
                      setState(() {
                        isSending = true;
                      });
                      await Future.delayed(Duration(seconds: 60));
                    },
                    child: isSending
                        ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator(
                              color: Colors.white,
                            )),
                        const SizedBox(
                          width: 24,
                        ),
                        Text('Please Wait...')
                      ],
                    )
                        : Text('Save'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> addNote() async {
    final pref = await SharedPreferences.getInstance();
    String idUser = pref.get('id');
    print(idUser);
    // print(_titleController.text);
    // print(bodyContent);
    final response = await http.get(Uri.parse(InfixApi.storeNoteBoard(
        int.parse(idUser), _titleController.text, bodyContent.toString())));

    // print(Uri.parse(InfixApi.storeNoteBoard(int.parse(idUser), _titleController.text, bodyContent.toString())));
    print(response.body);
    Navigator.pop(context);
  }
}
